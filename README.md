# ZeroTwo
Electron - Vue.js Anime Tracker

> Taiga alternative

#### Build status

[![Build Status](https://travis-ci.org/NicoAiko/zerotwo.svg?branch=master)](https://travis-ci.org/NicoAiko/zerotwo)
[![Build status](https://ci.appveyor.com/api/projects/status/goacd72jf5oopi47?svg=true)](https://ci.appveyor.com/project/NicoAiko/zerotwo)

---

## What is ZeroTwo?

> Welcome to ZeroTwo, the whole new experience of managing the ~~MyAnimeList~~ AniList!
>
> With this program you will easily add, update or delete Anime from your list!
>
> You only have to login to ~~MyAnimeList~~ AniList and the fun can begin!",

## [Get the latest release here!](https://github.com/NicoAiko/zerotwo/releases)

## Discord Server

##### Join the ZeroTwo Discord Server!

[<img src="https://discordapp.com/assets/e4923594e694a21542a489471ecffa50.svg" width="350">](https://discord.gg/sTpR4Gw)

---

## [Documentation](https://github.com/NicoAiko/zerotwo/wiki)

#### Short Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build


# lint all JS/Vue component files in `src/`
npm run lint
```

#### Installation issues
For installation issues with the `electron-builder` you have to set the environment variable `ELECTRON_BUILDER_ALLOW_UNRESOLVED_DEPENDENCIES` to `true`
```bash
# sample for unix based shells
ELECTRON_BUILDER_ALLOW_UNRESOLVED_DEPENDENCIES="true" npm i

# sample for windows powershell
$env:ELECTRON_BUILDER_ALLOW_UNRESOLVED_DEPENDENCIES="true"; npm i
```

---

# [LICENSE](LICENSE)

GNU General Public License v3.0
