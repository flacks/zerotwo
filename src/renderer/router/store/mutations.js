export default {
  setRoute(state, payload) {
    state.route = payload;
  },
  setFirstLoad(state, payload) {
    state.firstLoad = payload;
  },
};
