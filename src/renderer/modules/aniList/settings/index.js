// This file contains the headers and its width percentage
const HEADERS = Object.freeze([
  'HEADER_TITLE',
  'HEADER_PROGRESS',
  'HEADER_SCORE',
  'HEADER_SEASON',
  'HEADER_LASTUPDATED',
  'HEADER_GENRE',
]);

const WIDTHS = Object.freeze({
  HEADER_TITLE: 45,
  HEADER_PROGRESS: 15,
  HEADER_SCORE: 5,
  HEADER_SEASON: 12.5,
  HEADER_LASTUPDATED: 20,
  HEADER_GENRE: 10,
});

export default {
  HEADERS,
  WIDTHS,
};
