import Main from './pages/Main.vue';

export default [
  {
    path: '/torrents/main',
    name: 'Torrents-Main',
    component: Main,
  },
];
