// import generateMyAnimeList from './myanimelist';
import generateAniList from './aniList';

// const myAnimeList = generateMyAnimeList();
const aniList = generateAniList();

export default {
  // ...myAnimeList,
  ...aniList,
};
