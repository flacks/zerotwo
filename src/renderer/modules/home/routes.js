import Home from './pages/Home';

export default [
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
];
